<?php
/**
 * Created by PhpStorm.
 * User: Султановак
 * Date: 13.03.2020
 * Time: 21:23
 */

namespace Step\Controllers;


use Klein\Request;
use Klein\Response;

class SiteController
{
    function index(Request $request, Response $response)
    {
        //var_dump($request->params());

        return $response->code(404);
    }
}
