<?php

use Klein\Request;
use Klein\Response;

function action($name, Request $request = null, Response $response = null)
{
    $name = explode('@', $name);
    $controller = "Step\Controllers\\" . $name[0] . "Controller";
    $action = $name[1];

    $class = new $controller();
    return call_user_func([$class, $action], $request, $response);
}