<?php
/**
 * Created by PhpStorm.
 * User: Султановак
 * Date: 13.03.2020
 * Time: 20:40
 */

namespace Step;


use Klein\Klein;

class App
{
    public function __construct()
    {
        $this->mapRoutes();
    }

    protected function mapRoutes()
    {
        $router = new Klein();
        include_once "../routes/web.php";
        $router->dispatch();
    }
}